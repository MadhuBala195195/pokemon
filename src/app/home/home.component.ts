import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../shared/services/pokemon.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  pokemonData: any;
  data:any;
  nextUrl= "https://pokeapi.co/api/v2/pokemon?offset=20&limit=20";
  previousUrl:any;
  previousBtnFlag: any;

  constructor(private pokemonService: PokemonService) { }
  ngOnInit(): void {
    this.pokemonService.getData().subscribe({
      next: (v:any)=> {
        this.data = v.results;
      },
      error : (err)=> {
        console.log(err);
      }
    })
  }

  getPokemonData(data:any) {
    this.pokemonService.pokemonData(data).subscribe({
      next: (v:any)=> {
        debugger;
        console.log(v);
        this.pokemonData = v;
      },
      error : (err)=> {
        console.log(err);
      }
    })
  }
  nextData() {
    this.pokemonService.nextPage(this.nextUrl).subscribe({
      next: (v:any)=> {
        console.log(v);
        this.data = v.results;
        this.nextUrl = v.next;
        this.previousUrl = v.previous;
        this.previousBtnFlag = false;
      },
      error : (err)=> {
        console.log(err);
      }
    })
  }
  previousData() {
    this.pokemonService.previouPage(this.previousUrl).subscribe({
      next: (v:any)=> {
        console.log(v);
        this.data = v.results;
        this.nextUrl = v.next;
        this.previousUrl = v.previous;
      },
      error : (err)=> {
        console.log(err);
      }
    })
  }
}
