import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) { 
  }
  getData(){
    return this.http.get("https://pokeapi.co/api/v2/pokemon");
  }
  pokemonData(data:any) {
    let url = "https://pokeapi.co/api/v2/pokemon/"+data;
    return this.http.get(url);
  }
  nextPage(url:any) {
    return this.http.get(url);
  }
  previouPage(url:any) {
    return this.http.get(url);
  }
}
